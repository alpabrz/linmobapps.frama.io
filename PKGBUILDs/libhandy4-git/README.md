# libhandy4-git

This is a PKGBUILD to build and install libhandy for GTK4. It's based on a [previous upstream PKGBUILD](https://aur.archlinux.org/packages/libhandy4-git/).
