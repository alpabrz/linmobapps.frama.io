#!/bin/bash
echo "In order to use this install script make sure to have git and pip installed. To run it, make sure to have mpv installed."
# Getting your username and adjusting .desktop
username=$(whoami)
mkdir -p ~/.local/share/applications
echo "[Desktop Entry]" > ~/.local/share/applications/youplay.desktop
echo "Name=YouPlay" >> ~/.local/share/applications/youplay.desktop
echo "GenericName=Play music from YouTube" >> ~/.local/share/applications/youplay.desktop
echo "Comment=Play and download music from YouTube" >> ~/.local/share/applications/youplay.desktop
echo "Exec=python3 /home/${username}/.local/opt/youplay/youplay.py gui" >> ~/.local/share/applications/youplay.desktop
echo "Terminal=false" >> ~/.local/share/applications/youplay.desktop
echo "Type=Application" >> ~/.local/share/applications/youplay.desktop
echo "StartupNotify=false" >> ~/.local/share/applications/youplay.desktop
echo "Icon=/home/${username}/.local/opt/youplay/youplay.svg" >> ~/.local/share/applications/youplay.desktop
echo "Categories=GNOME;GTK;Utility;Internet;Music" >> ~/.local/share/applications/youplay.desktop
echo "Keywords=Multimedia;Music;Internet;YouTube" >> ~/.local/share/applications/youplay.desktop
echo "Path=/home/${username}/.local/opt/youplay" >> ~/.local/share/applications/youplay.desktop
echo "Hidden=false" >> ~/.local/share/applications/youplay.desktop
# Download and install YouPlay
mkdir -p ~/.local/opt/
cd  ~/.local/opt/
git clone https://codeberg.org/ralfhersel/youplay.git
# Symlink to make this work in bash, zsh etc.
ln -s ~/.local/opt/youplay/youplay.py ~/.local/bin/youplay.py
# Add it to the path
echo "export PATH=$PATH:/home/${username}/.local/bin" >> ~/.bashrc
# Add upgrade script
echo "#!/bin/bash" > ~/.local/bin/youplay-upgrade
echo "echo "Upgrading pip packages"" >> ~/.local/bin/youplay-upgrade
echo "pip3 install --user --upgrade youtube-dl" >> ~/.local/bin/youplay-upgrade
echo "pip3 install --user --upgrade python-mpv" >> ~/.local/bin/youplay-upgrade
echo "cd ~/.local/opt/youplay" >> ~/.local/bin/youplay-upgrade
echo "echo "Upgrade YouPlay"" >> ~/.local/bin/youplay-upgrade
echo "git pull" >> ~/.local/bin/youplay-upgrade
chmod +x ~/.local/bin/youplay-upgrade
# Add uninstaller
echo "#!/bin/bash" > ~/.local/bin/youplay-uninstall
echo "echo "Uninstalling YouPlay"" >> ~/.local/bin/youplay-uninstall
echo "rm -rf ~/.local/opt/youplay" >> ~/.local/bin/youplay-uninstall
echo "rm -f ~/.local/share/applications/youplay.desktop" >> ~/.local/bin/youplay-uninstall
echo "echo "To complete uninstall youplay, make sure to remove the installed pip packages 'youtube-dl' and 'python-mpv' if you no longer need them."" >> ~/.local/bin/youplay-uninstall
chmod +x ~/.local/bin/youplay-uninstall
# install dependencies and upgrade them if they are already installed.
pip3 install --user --upgrade youtube-dl
pip3 install --user --upgrade python-mpv
# adding a symlink in your Music folder, replace this with xdg_stuff
ln -s ~/.local/opt/youplay/music ~/Music/YouPlay
echo "If you did not see any errors, this should have worked. To update run 'youplay-upgrade'."
