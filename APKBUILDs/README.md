# Experimental APKBUILDs

This folder contains experimental APKBUILDs for postmarketOS packages that are not available upstream.

Please use with care and report or fix APKBUILD related issues. Thank you!

## Upstreaming welcome!

If it works for you and you have checked that it complies with the guidelines, feel free to submit my APKBUILDs upstream!

## Um,  what do I do with this?

If you are like me and are new to building packages for Alpine/postmarketOS, make sure to read the following pages of documentation:

* [pmbootstrap#Developing Packages with pmbootstrap](https://wiki.postmarketos.org/wiki/Pmbootstrap#Developing_packages_with_pmbootstrap) for the initial setup of your package development folder and how to build a package and what to do before submitting information,
* [packaging](https://wiki.postmarketos.org/wiki/Packaging) for general packaging instructions,
* [Build internals](https://wiki.postmarketos.org/wiki/Build_internals) with more information on what pmbootstrap build does behind the scenes,
* [pmbootstrap Repo readme](https://gitlab.com/postmarketOS/pmbootstrap), the "Packages" section has helpful hints.

If you want to build packages for your ARM64 device, the command then looks something like this for `notorious`:

`pmbootstrap build --force --strict --arch=aarch64 notorious` 

The resulting .apk file can then be found in `~/.local/var/pmbootstrap/packages/edge/aarch64/`. 

After copying it over to you postmarketOS phone, you can install it there by running `sudo apk add --allow-untrusted FILENAME`.
